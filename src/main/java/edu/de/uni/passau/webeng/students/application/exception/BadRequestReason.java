package edu.de.uni.passau.webeng.students.application.exception;

public enum BadRequestReason {

    NOT_ExISTING,
    ALREADY_IN_COURSE

}
