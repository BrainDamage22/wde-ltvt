package edu.de.uni.passau.webeng.students.web.dto;

import edu.de.uni.passau.webeng.students.model.Course;
import edu.de.uni.passau.webeng.students.model.Student;

public class DtoConverter {

    public static StudentDto convertToDto (Student student){
        return new StudentDto(Long.parseLong(student.getMatrNr()),student.getFirstName(),student.getLastName());
    }

    public static CourseDto convertToDto(Course course ){
        return new CourseDto(course.getId(),course.getTitle(),course.getDescription());
    }
}
