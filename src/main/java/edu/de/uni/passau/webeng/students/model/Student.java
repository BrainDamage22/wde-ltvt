package edu.de.uni.passau.webeng.students.model;

import java.util.List;

public class Student {
	private String matrNr;
	private String firstName;
	private String lastName;
	private List<Course> courses;
	private List<Course> finishedCourses;

	public Student(String mtrNumber, String firstName, String lastName, List<Course> courses, List<Course> finishedCourses) {
		this.matrNr = mtrNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.courses =courses;
		this.finishedCourses = finishedCourses;
	}

	public String getMatrNr() {
		return matrNr;
	}

	public void setMatrNr(String matrNr) {
		this.matrNr = matrNr;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public List<Course> getFinishedCourses() {
		return finishedCourses;
	}

	public void setFinishedCourses(List<Course> finishedCourses) {
		this.finishedCourses = finishedCourses;
	}
}