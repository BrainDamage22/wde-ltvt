package edu.de.uni.passau.webeng.students.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.de.uni.passau.webeng.students.application.service.StudentService;
import edu.de.uni.passau.webeng.students.web.dto.StudentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    /**
     *
     * @param id
     * @return studentDto Object
     *
     * method to get a studentDto Object from student id
     */
    @GetMapping(value = "/{id}")
    public StudentDto getStudend(@PathVariable Long id){
        return studentService.getStudent(id);
    }

    /**
     *
     * @param id
     * @return List of courses as JSON-String
     * @throws JsonProcessingException
     *
     * method to get a JSON-String of a students courses by student id
     */
    @GetMapping(value = "/{id}/courses")
    public String getCourses(@PathVariable Long id) throws JsonProcessingException {
        return StudentService.getCourses(id);
    }

    /**
     *
     * @param id
     * @param cid
     *
     * Trys to add a student to a course by student and course ids
     */
    @PostMapping(value="/{id}/courses/{cid}")
    public void registerInCourse(@PathVariable Long id, @PathVariable String cid){
        StudentService.registerInCourse(id,cid);
    }

}
