package edu.de.uni.passau.webeng.students.application.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.de.uni.passau.webeng.students.application.exception.BadRequestException;
import edu.de.uni.passau.webeng.students.application.exception.BadRequestReason;
import edu.de.uni.passau.webeng.students.application.exception.MissingPrerequisiteException;
import edu.de.uni.passau.webeng.students.model.Course;
import edu.de.uni.passau.webeng.students.model.Student;
import edu.de.uni.passau.webeng.students.web.dto.DtoConverter;
import edu.de.uni.passau.webeng.students.web.dto.StudentDto;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class StudentService {

	private static List<Student> students = new ArrayList<>();
	private static List<Course> courses = new ArrayList<>();

	static {
	    // Initializes the data of our model
	    Course course0 = new Course("c0", "JSF", "Learn JSF.", null);
	    Course course1 = new Course("c1", "Maven", "One of the most popular dependency management tools.", null);
	    Course course2 = new Course("c2", "Web Servers", "Learn about web servers.", null);

	    List<Course> courseList0 = new LinkedList<>();
	    courseList0.add(course1);
        courseList0.add(course2);
	    Course course3 = new Course("c3", "Spring Boot", "Use Spring Boot to bootstrap servers.", courseList0);

        List<Course> courseList1 = new LinkedList<>();
        courseList1.addAll(courseList0);
        courseList1.add(course3);
	    Course course4 = new Course("c4", "Spring Data", "A course about data persistence on the server.", courseList1);

	    courses.add(course0);
	    courses.add(course1);
	    courses.add(course2);
	    courses.add(course3);
	    courses.add(course4);

        List<Course> courseList2 = new LinkedList<>();
        courseList2.add(course0);
        courseList2.add(course1);

        List<Course> courseList3 = new LinkedList<>();
        courseList3.add(course3);

        List<Course> courseList4 = new LinkedList<>();
        courseList4.add(course0);

	    Student student0 = new Student("23328", "Max", "Muster", courseList2, null);
	    Student student1 = new Student("34622", "Hans", "Muster", courseList3, courseList0);
	    Student student2 = new Student("48645", "Alice", "Klint", courseList4, courseList0);
	    Student student3 = new Student("24232", "Bob", "Ser", null, courseList1);

	    students.add(student0);
	    students.add(student1);
	    students.add(student2);
	    students.add(student3);
	}

	/**
	 *
	 * @param id
	 * @return Json-String with list of courses
	 * @throws JsonProcessingException
	 *
	 * method to get a json-string with a list of all courses of a student with the students id
	 */
	public static String getCourses(Long id) throws JsonProcessingException {
		Student s = searchStudent(id);
		List<Course> courses = null;

		//check if student exists
		if (s != null) {
			courses = s.getCourses();
		}
		else {
			throw new BadRequestException(BadRequestReason.NOT_ExISTING);
		}

		ObjectMapper mapper = new ObjectMapper();

		// returns Json-String
		return mapper.writeValueAsString(courses);
	}

	/**
	 *
	 * @param id
	 * @return StudentDto
	 *
	 * method to get a studentDto with the students id
	 */
	public StudentDto getStudent(Long id) {
		Student s = searchStudent(id);

		//check if student exists
		if (s == null) {
			throw new BadRequestException(BadRequestReason.NOT_ExISTING);
		} else {
			return DtoConverter.convertToDto(s);
		}
	}


	/**
	 *
	 * @param id
	 * @param cid
	 *
	 * method to register a student to a course with the students id and the cours id
	 * checks if student meets requirements of the course and throws a MissingPrerequisiteException if not
	 */
	public static void registerInCourse(Long id, String cid){
		Student s = searchStudent(id);
		Course c = searchCourse(cid);

		//check if student and course exist
		if(s == null){
			throw new BadRequestException(BadRequestReason.NOT_ExISTING);
		}else if (c == null){
			throw new BadRequestException(BadRequestReason.NOT_ExISTING);
		} else {

			//checks if student is already in course
			//Possible: checking if student already visited this course (not required in exercise)
			for (Course cour : s.getCourses()) {
				if (c.getTitle().equals(cour.getTitle())) {
					throw new BadRequestException(BadRequestReason.ALREADY_IN_COURSE);
			}}
			//Test if the course has prerequisites
			 if(c.getPrerequisites() == null){
				 List<Course> courses = s.getCourses();
				 courses.add(c);
				 s.setCourses(courses);
			 }else{
				int prereq_size = c.getPrerequisites().size();
				int found_prereq = 0;
				boolean found = false;
				String missing = "";

				//check if student meets requirements of course
				for (Course prerequisite : c.getPrerequisites()) {
					for (Course cours : s.getCourses()) {
						if (cours.getTitle().equals(prerequisite.getTitle())) {
							found_prereq += 1;
							found = true;
						}
					}
					if (!found) {
						missing = missing + prerequisite.getTitle();
					}

					//adding the student to course if requirements are met
					if (found_prereq == prereq_size) {
						List<Course> courses = s.getCourses();
						courses.add(c);
						s.setCourses(courses);
					}
					//throwing axception if student doesnt meet the requirements
					else throw new MissingPrerequisiteException(missing);
				}
			}
		}

	}

	/**
	 *
	 * @param id
	 * @return student s or null if not found
	 *
	 * method to get student object by id
	 */
	private static Student searchStudent(Long id){
			for(Student s : students) {
				if (Long.parseLong(s.getMatrNr()) == id) {
					return s;
				}
			}
			return  null;
	}

	/**
	 *
	 * @param cid
	 * @return course c or null if not found
	 *
	 * method to get course by id
	 */
	private static Course searchCourse(String cid){
		for(Course c: courses) {
			if (c.getId().equals(cid)) {
				return c;
			}
		}
		return  null;
	}
}