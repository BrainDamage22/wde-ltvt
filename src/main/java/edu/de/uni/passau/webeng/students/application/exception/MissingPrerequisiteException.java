package edu.de.uni.passau.webeng.students.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.logging.Logger;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MissingPrerequisiteException extends RuntimeException {
    private static final Logger log = Logger.getLogger( BadRequestException.class.getName() );

    public MissingPrerequisiteException(String message) {
        super("Student needs to finish: " + message);
        log.config("Student needs to finish: " + message);
    }
}
